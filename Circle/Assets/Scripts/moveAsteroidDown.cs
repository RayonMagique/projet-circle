﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveAsteroidDown : MonoBehaviour
{
  private float sizeY;
  private Vector2 movement;
  private Vector3 basGauche;

  // Start is called before the first frame update
  void Start()
  {
    //Bouge l'asteroide vers le bas avec une vitesse de 2
    movement = new Vector2(0, -2);
    GetComponent<Rigidbody2D>().velocity = movement;
    sizeY = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;
    basGauche = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0)); 
  }

  // Update is called once per frame
  void Update()
  {
    //Si l'asteroide sort de l'écran , il est détruit
    if (transform.position.y < basGauche.y - (sizeY / 2)){
			Destroy(gameObject);
		}
  }
}
