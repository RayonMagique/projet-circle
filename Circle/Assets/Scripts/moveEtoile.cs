﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveEtoile : MonoBehaviour
{
  private float sizeY;
  private Vector2 movement;

  // Start is called before the first frame update
  void Start()
  {
    //Bouge l'étoile en diagonale avec une vitesse de 1 en x et de 1 en y
    movement = new Vector2(1, 1);
    GetComponent<Rigidbody2D>().velocity = movement;
  }

  // Update is called once per frame
  void Update()
  {
  }
}
