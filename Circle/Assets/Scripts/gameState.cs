﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameState : MonoBehaviour {
  private static gameState _instance;

	private int scorePlayer = 0 ; 

  public static gameState Instance { get { return _instance; } }

  private void Awake(){
    if (_instance == null){
      _instance = this;
      DontDestroyOnLoad(this.gameObject);

      //Rest of your Awake code

    } else {
      Destroy(this);
    }
  }

	void FixedUpdate(){
    //On met à jour l'affichage du score et highscore
    if(GameObject.FindWithTag("scoreLabel")){
		  GameObject.FindWithTag("scoreLabel").GetComponent<Text>().text = "" + scorePlayer;
    }
    if(GameObject.FindWithTag("highScoreLabel")){
      GameObject.FindWithTag("highScoreLabel").GetComponent<Text>().text = "" + getHighScore(gameStateDifficulty.Instance.getDifficulty());
    }
  }

  //Ajoute des points au score
	public void addScorePlayer(int toAdd) {
		scorePlayer += toAdd;
	}

  //Set le score à une certaine valeur
  public void setScorePlayer(int score){
    scorePlayer = score;
  }

  //Retourne le score
	public int getScorePlayer(){
		return scorePlayer;
	}

  //Set le highscore correspondant au niveau de difficulté avec le score courant du joueur
  public void setHighScore(int lvl){
    if(lvl == 1){
      if(scorePlayer>PlayerPrefs.GetInt("HighScore_Facile", 0)){
       PlayerPrefs.SetInt("HighScore_Facile", this.scorePlayer);
      }
    }
    else if(lvl == 2){
      if(scorePlayer>PlayerPrefs.GetInt("HighScore_Normal", 0)){
        PlayerPrefs.SetInt("HighScore_Normal", this.scorePlayer);
      }
    }
    else{
      if(scorePlayer>PlayerPrefs.GetInt("HighScore_Difficile", 0)){
        PlayerPrefs.SetInt("HighScore_Difficile", this.scorePlayer);
      }
    }
  }

  //Retourne le highscore correspondant au niveau de difficulté
  public int getHighScore(int lvl){
    if(lvl == 1){
      return PlayerPrefs.GetInt("HighScore_Facile", 0);
    }
    else if(lvl == 2){
      return PlayerPrefs.GetInt("HighScore_Normal", 0);
    }
    else{
      return PlayerPrefs.GetInt("HighScore_Difficile", 0);
    }
  }	
}
