﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameStateDifficulty : MonoBehaviour {
  private static gameStateDifficulty _instance;

	private int difficulty = 0 ; 

  public static gameStateDifficulty Instance { get { return _instance; } }

  private void Awake(){
    if (_instance == null){
      _instance = this;
      DontDestroyOnLoad(this.gameObject);

      //Rest of your Awake code

    } else {
      Destroy(this);
    }
  }

  //Set la difficulté
	public void setDifficulty(int lvl) {
		difficulty = lvl;
	}

  //Retourne la difficulté
	public int getDifficulty(){
		return difficulty;
	}	
}
