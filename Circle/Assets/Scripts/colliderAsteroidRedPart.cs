﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colliderAsteroidRedPart : MonoBehaviour
{
  public GameObject etoile;
  public GameObject explosion;
  private GameObject clone_explosion;
  private GameObject clone_etoile;

  // Start is called before the first frame update
  void Start()
  {
      
  }

  // Update is called once per frame
  void Update()
  {
      
  }

  void OnTriggerEnter2D(Collider2D collider){
    //Si le demi-cercle bleu touche un astéroide	
    if(collider.tag == "asteroid"){
      //Si c'est un asteroide rouge	
      if(collider.GetComponent<SpriteRenderer>().sprite.ToString() == "red_asteroid (UnityEngine.Sprite)"){
        //On détruit l'asteroide
        Destroy(collider.gameObject);
        //On joue le son
        GetComponent<AudioSource>().Play();
        //On instancie une etoile 
        clone_etoile = Instantiate(etoile, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity) as GameObject;
        //On donne la capacité à l'étoile de bouger
        clone_etoile.AddComponent<moveEtoile>();
        //On donne la capacité à l'étoile de disparaitre
        clone_etoile.AddComponent<fadeOut>();
        //On ajoute 100 points au score
        gameState.Instance.addScorePlayer(100); 
      }
      else{
        //Sinon, on supprime les gameObject life
        if(GameObject.FindGameObjectWithTag("life_3")){
          GameObject.FindGameObjectWithTag("life_3").AddComponent<fadeOut>();
        }
        else if(GameObject.FindGameObjectWithTag("life_2")){	
          GameObject.FindGameObjectWithTag("life_2").AddComponent<fadeOut>();
        }
        else{	
          GameObject.FindGameObjectWithTag("life_1").AddComponent<fadeOut>();
          //Si on perd la dernière vie, on instancie une explosion
          clone_explosion = Instantiate(explosion, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity) as GameObject;
          //On joue le son de l'explosion
          GameObject.FindGameObjectWithTag("explosion").GetComponent<AudioSource>().Play();
          //On détruit le cercle
          Destroy(GameObject.FindWithTag("circle"));	
        }
      }
    }
  }
}


